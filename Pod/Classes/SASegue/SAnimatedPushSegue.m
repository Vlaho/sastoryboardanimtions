//
//  SAPushSegue.m
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/11/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "SAnimatedPushSegue.h"
#import "StoryboardAnimtions.h"

@implementation SAnimatedPushSegue

-(void)perform
{
    UIViewController *svc = self.sourceViewController;
    UIViewController *dvc = self.destinationViewController;
    [StoryboardAnimtions hideSubviewFromView:dvc.view animated:NO completion:NULL];
    [StoryboardAnimtions hideSubviewFromView:svc.view animated:YES completion:^{
        [svc.navigationController pushViewController:dvc animated:NO];
        [StoryboardAnimtions showSubviewFromView:dvc.view animated:YES completion:NULL];
    }];
}

@end
