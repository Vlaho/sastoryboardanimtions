//
//  SAModalSegue.m
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/11/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "SAnimatedModalSegue.h"
#import "StoryboardAnimtions.h"

@implementation SAnimatedModalSegue

-(void)perform
{
    UIViewController *svc = self.sourceViewController;
    UIViewController *dvc = self.destinationViewController;
    [StoryboardAnimtions hideSubviewFromView:dvc.view animated:NO completion:NULL];
    [StoryboardAnimtions hideSubviewFromView:svc.view animated:YES completion:^{
        [svc presentViewController:dvc animated:NO completion:^{
            [StoryboardAnimtions showSubviewFromView:dvc.view animated:YES completion:NULL];
        }];
    }];
}

@end
