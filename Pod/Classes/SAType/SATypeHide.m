//
//  SATypeHide.m
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/11/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "SATypeHide.h"

@implementation SATypeHide

-(NSArray *)groupedModelsSortedByOrderNumber:(NSArray *)groupedModels
{
    return [[groupedModels reverseObjectEnumerator] allObjects];
}

-(void)runNonAnimatedWithModel:(SAModel *)model
{
    [model hideNotAnimated];
}

-(void)runAnimatedWithModel:(SAModel *)model completion:(void (^)())completion
{
    [model hideAnimatedWithCompletion:completion];
}

@end
