//
//  SAAppDelegate.h
//  SAStoryboardAnimtions
//
//  Created by Vlaho on 08/22/2015.
//  Copyright (c) 2015 Vlaho. All rights reserved.
//

@import UIKit;

@interface SAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
