//
//  SAViewController.m
//  SAStoryboardAnimtions
//
//  Created by Vlaho on 08/22/2015.
//  Copyright (c) 2015 Vlaho. All rights reserved.
//

#import "SAViewController.h"
#import <SAStoryboardAnimtions/StoryboardAnimtions.h>

@interface SAViewController ()

@end

@implementation SAViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.presentingViewController) {//if is presented
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [StoryboardAnimtions hideSubviewFromView:self.view animated:YES completion:^{
                [self dismissViewControllerAnimated:NO completion:nil];
            }];
        });
    }
}

@end
