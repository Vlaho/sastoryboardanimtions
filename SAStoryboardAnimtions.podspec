#
# Be sure to run `pod lib lint SAStoryboardAnimtions.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "SAStoryboardAnimtions"
  s.version          = "0.1.0"
  s.summary          = "Animate views using IBInspectable params."

  s.description      = <<-DESC
    This is easy to use animation manager. It uses 'pop' for all animations so it can be used with auto-layout.
    To start using it joust click on some paramethers in storyboard and call it with a simple one-liner form 'StoryboardAnimtions.h'.
                       DESC

  s.homepage         = "https://bitbucket.org/Vlaho/sastoryboardanimtions"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Vlaho" => "vlaho.poluta@infinum.hr" }
  s.source           = { :git => "https://Vlaho@bitbucket.org/Vlaho/sastoryboardanimtions.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'SAStoryboardAnimtions' => ['Pod/Assets/*.png']
  }

  s.frameworks = 'UIKit'
  s.dependency 'pop'
end
