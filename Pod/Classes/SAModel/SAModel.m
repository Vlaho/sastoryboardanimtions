//
//  SAModel.m
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/10/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "SAModel.h"
#import "UIView+StoryboardAnimations.h"

@implementation SAModel

+(instancetype)modelWithView:(UIView *)view shouldAnimate:(BOOL)shouldAnimate shouldHide:(BOOL)shouldHide animateSubviews:(BOOL)animateSubviews x:(CGFloat)x y:(CGFloat)y scale:(CGFloat)scale radians:(CGFloat)radians orderNumber:(NSInteger)orderNumber
{
    SAModel *model = [SAModel new];
    model.referencingView = view;
    model.shouldAnimate = shouldAnimate;
    model.shouldHide = shouldHide;
    model.animateSubviews = animateSubviews;
    model.x = x;
    model.y = y;
    model.scale = scale;
    model.radians = radians;
    model.orderNumber = orderNumber;
    return model;
}

+(instancetype)modelFromView:(UIView *)view
{
    return [SAModel modelWithView:view shouldAnimate:!view.doNotAnimate shouldHide:YES animateSubviews:view.animateSubviews x:[SAModel xFromPosition:view.translationDirection] y:[SAModel yFromPosition:view.translationDirection] scale:view.scaleFactor radians:[SAModel radiansFromDegrees:view.degrees] orderNumber:view.orderNumber];
}

+(CGFloat)radiansFromDegrees:(CGFloat)degree
{
    return degree / 180. * M_PI;
}

+(CGFloat)xFromPosition:(NSInteger)position
{
    if (position == 1 || position == 4 || position == 7) {
        return -1000;
    }
    if (position == 3 || position == 6 || position == 9) {
        return 1000;
    }
    return 0;
}

+(CGFloat)yFromPosition:(NSInteger)position
{
    if (position == 1 || position == 2 || position == 3) {
        return -1000;
    }
    if (position == 7 || position == 8 || position == 9) {
        return 1000;
    }
    return 0;
}

-(NSString *)description
{
    return [self dictionaryWithValuesForKeys:@[@"referencingView",
                                               @"shouldAnimate",
                                               @"shouldHide",
                                               @"animateSubviews",
                                               @"x",
                                               @"y",
                                               @"scale",
                                               @"radians",
                                               @"orderNumber",
                                               ]].description;
}

@end
