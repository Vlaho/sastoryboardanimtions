//
//  StoryboardAnimtions.h
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/10/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIView+StoryboardAnimations.h"

#import "SAModel.h"
#import "SAModel+ShowHide.h"
#import "SAModel+Grouping.h"

@interface StoryboardAnimtions : NSObject

+(void)showView:(UIView *)view animated:(BOOL)animated completion:(void(^)())completion;
+(void)hideView:(UIView *)view animated:(BOOL)animated completion:(void(^)())completion;

+(void)showViews:(NSArray *)views animated:(BOOL)animated completion:(void(^)())completion;
+(void)hideViews:(NSArray *)views animated:(BOOL)animated completion:(void(^)())completion;

+(void)showSubviewFromView:(UIView *)view animated:(BOOL)animated completion:(void(^)())completion;
+(void)hideSubviewFromView:(UIView *)view animated:(BOOL)animated completion:(void(^)())completion;

//Those metodes owerride your work in views

+(void)showView:(UIView *)view animated:(BOOL)animated modelForView:(SAModel *(^)(UIView *view))modelForView completion:(void(^)())completion;
+(void)hideView:(UIView *)view animated:(BOOL)animated modelForView:(SAModel *(^)(UIView *view))modelForView completion:(void(^)())completion;
+(void)showViews:(NSArray *)views animated:(BOOL)animated modelForView:(SAModel *(^)(UIView *view))modelForView completion:(void(^)())completion;
+(void)hideViews:(NSArray *)views animated:(BOOL)animated modelForView:(SAModel *(^)(UIView *view))modelForView completion:(void(^)())completion;
+(void)showSubviewFromView:(UIView *)view animated:(BOOL)animated modelForSubview:(SAModel *(^)(UIView *view))modelForSubview completion:(void(^)())completion;
+(void)hideSubviewFromView:(UIView *)view animated:(BOOL)animated modelForSubview:(SAModel *(^)(UIView *view))modelForSubview completion:(void(^)())completion;

@end
