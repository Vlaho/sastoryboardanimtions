//
//  UIView+StoryboardAnimations.h
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/10/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSInteger {
    TranslationDirectionUpLeft      = 1,
    TranslationDirectionUp          = 2,
    TranslationDirectionUpRight     = 3,
    TranslationDirectionLeft        = 4,
    TranslationDirectionCenter      = 5,//None
    TranslationDirectionRight       = 6,
    TranslationDirectionBottomLeft  = 7,
    TranslationDirectionBottom      = 8,
    TranslationDirectionBottomRight = 9
} TranslationDirection;


@interface UIView (StoryboardAnimations)

//defoult = NO
@property (nonatomic, assign) IBInspectable BOOL doNotAnimate;
//defoult = TranslationDirectionCenter = 5
@property (nonatomic, assign) IBInspectable NSInteger translationDirection;
//defoult = 0
@property (nonatomic, assign) IBInspectable CGFloat degrees;
//defoult = 1
@property (nonatomic, assign) IBInspectable CGFloat scaleFactor;
//defoult = 0
@property (nonatomic, assign) IBInspectable NSInteger orderNumber;
//defoult = NO
@property (nonatomic, assign) IBInspectable BOOL animateSubviews;

@end
