//
//  SAModel.h
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/10/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SAModel : NSObject

@property (nonatomic, weak) UIView *referencingView;
@property (nonatomic, assign) BOOL shouldAnimate;
@property (nonatomic, assign) BOOL shouldHide;
@property (nonatomic, assign) BOOL animateSubviews;
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat scale;
@property (nonatomic, assign) CGFloat radians;
@property (nonatomic, assign) NSInteger orderNumber;

+(instancetype)modelFromView:(UIView *)view;
+(instancetype)modelWithView:(UIView *)view shouldAnimate:(BOOL)shouldAnimate shouldHide:(BOOL)shouldHide animateSubviews:(BOOL)animateSubviews x:(CGFloat)x y:(CGFloat)y scale:(CGFloat)scale radians:(CGFloat)radians orderNumber:(NSInteger)orderNumber;

@end
