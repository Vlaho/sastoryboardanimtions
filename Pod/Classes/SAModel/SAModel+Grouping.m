//
//  SAModel+Grouping.m
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/10/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "SAModel+Grouping.h"

@implementation SAModel (Grouping)

+(NSArray *)groupeModelsByOrderNumber:(NSArray *)models
{
    NSDictionary *modelDict = [SAModel setModelsInDictionaryOnOrderNumber:models];
    NSArray *dictKeys = [[modelDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    NSMutableArray *groupedModels = [NSMutableArray new];
    for (NSNumber *key in dictKeys) {
        NSArray *array = modelDict[key];
        [groupedModels addObject:array];
    }
    return groupedModels;
}

+(NSDictionary *)setModelsInDictionaryOnOrderNumber:(NSArray *)models
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    for (SAModel *model in models) {
        NSMutableArray *array = [SAModel arrayForKey:@(model.orderNumber) inDictionary:dict];
        [array addObject:model];
    }
    return dict;
}

+(NSMutableArray *)arrayForKey:(id)key inDictionary:(NSMutableDictionary *)dictionay
{
    NSMutableArray *array = [dictionay objectForKey:key];
    if (!array) {
        array = [NSMutableArray new];
        [dictionay setObject:array forKey:key];
    }
    return array;
}

@end
