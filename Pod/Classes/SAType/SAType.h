//
//  SAType.h
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/11/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAModel.h"
#import "SAModel+ShowHide.h"
#import "SAModel+Grouping.h"

@protocol SAType <NSObject>

-(NSArray *)groupedModelsSortedByOrderNumber:(NSArray *)groupedModels;
-(void)runNonAnimatedWithModel:(SAModel *)model;
-(void)runAnimatedWithModel:(SAModel *)model completion:(void(^)())completion;

@end
