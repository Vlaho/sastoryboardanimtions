//
//  StoryboardAnimtions.m
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/10/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "StoryboardAnimtions.h"
#import "SAType.h"
#import "SATypeShow.h"
#import "SATypeHide.h"

@implementation StoryboardAnimtions

#pragma mark - from view

+(void)hideView:(UIView *)view animated:(BOOL)animated completion:(void (^)())completion
{
    [StoryboardAnimtions hideView:view animated:animated modelForView:[StoryboardAnimtions modelForViewBlock] completion:completion];
}

+(void)showView:(UIView *)view animated:(BOOL)animated completion:(void (^)())completion
{
    [StoryboardAnimtions showView:view animated:animated modelForView:[StoryboardAnimtions modelForViewBlock] completion:completion];
}

+(void)hideSubviewFromView:(UIView *)view animated:(BOOL)animated completion:(void (^)())completion
{
    [StoryboardAnimtions hideSubviewFromView:view animated:animated modelForSubview:[StoryboardAnimtions modelForViewBlock] completion:completion];
}

+(void)showSubviewFromView:(UIView *)view animated:(BOOL)animated completion:(void (^)())completion
{
    [StoryboardAnimtions showSubviewFromView:view animated:animated modelForSubview:[StoryboardAnimtions modelForViewBlock] completion:completion];
}

+(void)hideViews:(NSArray *)views animated:(BOOL)animated completion:(void (^)())completion
{
    [StoryboardAnimtions hideViews:views animated:animated modelForView:[StoryboardAnimtions modelForViewBlock] completion:completion];
}

+(void)showViews:(NSArray *)views animated:(BOOL)animated completion:(void (^)())completion
{
    [StoryboardAnimtions showViews:views animated:animated modelForView:[StoryboardAnimtions modelForViewBlock] completion:completion];
}

+(SAModel *(^)(UIView *view))modelForViewBlock
{
    return  ^(UIView *view){return [SAModel modelFromView:view];};
}

#pragma mark - from model

+(void)hideView:(UIView *)view animated:(BOOL)animated modelForView:(SAModel *(^)(UIView *))modelForView completion:(void (^)())completion
{
    [StoryboardAnimtions hideViews:@[view] animated:animated modelForView:modelForView completion:completion];
}

+(void)showView:(UIView *)view animated:(BOOL)animated modelForView:(SAModel *(^)(UIView *))modelForView completion:(void (^)())completion
{
    [StoryboardAnimtions showViews:@[view] animated:animated modelForView:modelForView completion:completion];
}

+(void)hideSubviewFromView:(UIView *)view animated:(BOOL)animated modelForSubview:(SAModel *(^)(UIView *))modelForSubview completion:(void (^)())completion
{
    [StoryboardAnimtions hideViews:view.subviews animated:animated modelForView:modelForSubview completion:completion];
}

+(void)showSubviewFromView:(UIView *)view animated:(BOOL)animated modelForSubview:(SAModel *(^)(UIView *))modelForSubview completion:(void (^)())completion
{
    [StoryboardAnimtions showViews:view.subviews animated:animated modelForView:modelForSubview completion:completion];
}

+(void)hideViews:(NSArray *)views animated:(BOOL)animated modelForView:(SAModel *(^)(UIView *))modelForView completion:(void (^)())completion
{
    [StoryboardAnimtions performType:[SATypeHide new] onViews:views animated:animated modelForView:modelForView completion:completion];
}

+(void)showViews:(NSArray *)views animated:(BOOL)animated modelForView:(SAModel *(^)(UIView *))modelForView completion:(void (^)())completion
{
    [StoryboardAnimtions performType:[SATypeShow new] onViews:views animated:animated modelForView:modelForView completion:completion];
}

+(void)performType:(id<SAType>)type onViews:(NSArray *)views animated:(BOOL)animated modelForView:(SAModel *(^)(UIView *))modelForView completion:(void (^)())completion
{
    if (!modelForView) {
        return;
    }
    NSArray *models = [StoryboardAnimtions modelsOfViews:views withModelForSubview:modelForView];
    if (!animated) {
        [StoryboardAnimtions nonAnimatedWithType:type models:models completion:completion];
        return;
    }
    NSArray *groupedModels = [type groupedModelsSortedByOrderNumber:[SAModel groupeModelsByOrderNumber:models]];
    [self animatedWithType:type grupedModels:groupedModels completion:completion];
}

#pragma mark - animated

+(void)animatedWithType:(id<SAType>)type grupedModels:(NSArray *)groupedModels completion:(void (^)())completion
{
    [StoryboardAnimtions recursiveWithType:type addCompletionsToGrupedModels:groupedModels currnetIndex:0 completion:completion];
}

+(void)recursiveWithType:(id<SAType>)type addCompletionsToGrupedModels:(NSArray *)groupedModels currnetIndex:(NSInteger)currnetIndex completion:(void (^)())completion
{
    NSArray *models = groupedModels[currnetIndex];
    if (currnetIndex >= groupedModels.count - 1) {
        [StoryboardAnimtions animatedWithType:type models:models completion:completion];
        return;
    }
    [StoryboardAnimtions animatedWithType:type models:models completion:^{
        [StoryboardAnimtions recursiveWithType:type addCompletionsToGrupedModels:groupedModels currnetIndex:(currnetIndex + 1) completion:completion];
    }];
}

+(void)animatedWithType:(id<SAType>)type models:(NSArray *)models completion:(void (^)())completion
{
    __block NSInteger numBlocks = models.count;
    void (^subSuccess)(void) = ^(){
        numBlocks -= 1;
        if (numBlocks <= 0 && completion) {
            completion();
        }
    };
    for (SAModel *model in models) {
        [type runAnimatedWithModel:model completion:subSuccess];
    }
}

#pragma mark - not animated

+(void)nonAnimatedWithType:(id<SAType>)type models:(NSArray *)models completion:(void (^)())completion
{
    for (SAModel *model in models) {
        [type runNonAnimatedWithModel:model];
    }
    if (completion) {
        completion();
    }
}

+(void)nonAnimatedShowWithModels:(NSArray *)models completion:(void (^)())completion
{
    for (SAModel *model in models) {
        [model showNotAnimated];
    }
    if (completion) {
        completion();
    }
}

+(void)nonAnimatedHideWithModels:(NSArray *)models completion:(void (^)())completion
{
    for (SAModel *model in models) {
        [model hideNotAnimated];
    }
    if (completion) {
        completion();
    }
}

#pragma mark - model array manipulation

+(NSArray *)modelsOfViews:(NSArray *)views withModelForSubview:(SAModel *(^)(UIView *))modelForView
{
    NSMutableArray *modelsArray = [NSMutableArray new];
    [StoryboardAnimtions fillArray:modelsArray withModelsOfViews:views startOrderNumber:0 modelForView:modelForView];
    return modelsArray;
}

+(void)fillArray:(NSMutableArray *)modelArray withModelsOfViews:(NSArray *)views startOrderNumber:(NSInteger)startOrderNumber modelForView:(SAModel *(^)(UIView *))modelForView
{
    for (UIView  *view in views) {
        SAModel *model = modelForView(view);
        model.orderNumber += startOrderNumber;
        [modelArray addObject:model];
        if (model.animateSubviews) {
            [StoryboardAnimtions fillArray:modelArray withModelsOfViews:view.subviews startOrderNumber:(model.orderNumber + 1) modelForView:modelForView];
        }
    }
}

@end
