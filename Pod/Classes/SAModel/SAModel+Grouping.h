//
//  SAModel+Grouping.h
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/10/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "SAModel.h"

@interface SAModel (Grouping)

+(NSArray *)groupeModelsByOrderNumber:(NSArray *)models;

@end
