//
//  SATypeShow.m
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/11/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "SATypeShow.h"

@implementation SATypeShow

-(NSArray *)groupedModelsSortedByOrderNumber:(NSArray *)groupedModels
{
    return groupedModels;
}

-(void)runNonAnimatedWithModel:(SAModel *)model
{
    [model showNotAnimated];
}

-(void)runAnimatedWithModel:(SAModel *)model completion:(void (^)())completion
{
    [model showAnimatedWithCompletion:completion];
}

@end
