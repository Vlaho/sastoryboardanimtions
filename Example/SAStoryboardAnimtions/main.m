//
//  main.m
//  SAStoryboardAnimtions
//
//  Created by Vlaho on 08/22/2015.
//  Copyright (c) 2015 Vlaho. All rights reserved.
//

@import UIKit;
#import "SAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SAAppDelegate class]));
    }
}
