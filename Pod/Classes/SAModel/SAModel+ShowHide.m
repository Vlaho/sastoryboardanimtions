//
//  SAModel+ShowHide.m
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/10/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "SAModel+ShowHide.h"
#import <pop/POP.h>

@implementation SAModel (ShowHide)

#pragma mark - not animated

-(void)showNotAnimated
{
    CGAffineTransform trasform = CGAffineTransformIdentity;
    [self presentNotAnimatedWithAlpha:1 affineTransfor:trasform];
}

-(void)hideNotAnimated
{
    CGAffineTransform transform = CGAffineTransformConcat(CGAffineTransformConcat(CGAffineTransformMakeTranslation(self.x, self.y), CGAffineTransformMakeRotation(self.radians)), CGAffineTransformMakeScale(self.scale, self.scale));
    [self presentNotAnimatedWithAlpha:0 affineTransfor:transform];
}

-(void)presentNotAnimatedWithAlpha:(CGFloat)alpha affineTransfor:(CGAffineTransform)transform
{
    if (!self.shouldAnimate) {
        return;
    }
    if (self.shouldHide) {
        self.referencingView.alpha = alpha;
    }
    self.referencingView.layer.transform = CATransform3DMakeAffineTransform(transform);
}

#pragma makr - animated

-(void)showAnimatedWithCompletion:(void (^)())completion
{
    [self presentAnimatedWithTranslationPoint:CGPointMake(0, 0) alpha:1 radians:0 scale:1 completion:completion];
}

-(void)hideAnimatedWithCompletion:(void (^)())completion
{
    [self presentAnimatedWithTranslationPoint:CGPointMake(self.x, self.y) alpha:0 radians:self.radians scale:self.scale completion:completion];
}

-(void)presentAnimatedWithTranslationPoint:(CGPoint)point alpha:(CGFloat)alpha radians:(CGFloat)radians scale:(CGFloat)scale completion:(void (^)())completion
{
    if (!self.shouldAnimate) {
        if (completion) {
            completion();
        }
        return;
    }
    alpha = (self.shouldHide) ? alpha : 1;
    
    __block NSInteger animationsCount = 4;
    void (^subSuccess)(void) = ^(){
        animationsCount -= 1;
        if (animationsCount <= 0 && completion) {
            completion();
        }
    };
    
    [self transaltionWithPoint:point conpletion:subSuccess];
    [self transparencyWithAlpha:alpha conpletion:subSuccess];
    [self rotationWithRadians:radians conpletion:subSuccess];
    [self scailingWithScaleFactor:scale conpletion:subSuccess];
}

#pragma makr - pop animations

-(void)transaltionWithPoint:(CGPoint)point conpletion:(void(^)())completion
{
    POPSpringAnimation *animSpring = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerTranslationXY];
    animSpring.toValue = [NSValue valueWithCGPoint:point];
    [animSpring setCompletionBlock:^(POPAnimation *anim, BOOL success) {
        if (completion) { completion(); }
    }];
    [self.referencingView.layer pop_addAnimation:animSpring forKey:@"translate"];
}

-(void)transparencyWithAlpha:(CGFloat)alpha conpletion:(void(^)())completion
{
    POPBasicAnimation *animAlfa = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animAlfa.toValue = @(alpha);
    [animAlfa setCompletionBlock:^(POPAnimation *anim, BOOL success) {
        if (completion) { completion(); }
    }];
    [self.referencingView pop_addAnimation:animAlfa forKey:@"show"];
}

-(void)rotationWithRadians:(CGFloat)radians conpletion:(void(^)())completion
{
    POPSpringAnimation *animSpring = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerRotation];
    animSpring.toValue = @(radians);
    [animSpring setCompletionBlock:^(POPAnimation *anim, BOOL success) {
        if (completion) { completion(); }
    }];
    [self.referencingView.layer pop_addAnimation:animSpring forKey:@"rotate"];
}

-(void)scailingWithScaleFactor:(CGFloat)scaleFactor conpletion:(void(^)())completion
{
    POPSpringAnimation *animSpring = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    animSpring.toValue =[NSValue valueWithCGPoint:CGPointMake(scaleFactor, scaleFactor)];
    [animSpring setCompletionBlock:^(POPAnimation *anim, BOOL success) {
        if (completion) { completion(); }
    }];
    [self.referencingView.layer pop_addAnimation:animSpring forKey:@"scale"];
}

@end
