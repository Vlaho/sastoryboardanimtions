//
//  UIView+StoryboardAnimations.m
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/10/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "UIView+StoryboardAnimations.h"
#import <objc/runtime.h>

static char kAssociatedDoNotAnimate;
static char kAssociatedTranslationDirection;
static char kAssociatedDegrees;
static char kAssociatedScaleFactor;
static char kAssociatedOrderNumber;
static char kAssociatedAnimateSubviews;


@interface UIView (StoryboardAnimationsPrivate)

-(void)setBoolean:(BOOL)number withIdentifier:(char *)identifier;
-(void)setInteger:(NSInteger)number withIdentifier:(char *)identifier;
-(void)setFloat:(CGFloat)number withIdentifier:(char *)identifier;
-(BOOL)booleanForIdentifier:(char *)identifier defoult:(BOOL)defoult;
-(NSInteger)integerForIdentifier:(char *)identifier defoult:(NSInteger)defoult;
-(CGFloat)floatForIdentifier:(char *)identifier defoult:(CGFloat)defoult;

@end

@implementation UIView (StoryboardAnimationsPrivate)

#pragma mark - associations

- (void)setAssociatedNSNumber:(NSNumber *)object withIdentifier:(char *)identifier
{
    objc_setAssociatedObject(self, identifier, object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)associatedNsnuberFromIdentifier:(char *)identifier
{
    return objc_getAssociatedObject(self, identifier);
}

#pragma mark - proimtives setters

-(void)setBoolean:(BOOL)number withIdentifier:(char *)identifier
{
    [self setAssociatedNSNumber:@(number) withIdentifier:identifier];
}

-(void)setInteger:(NSInteger)number withIdentifier:(char *)identifier
{
    [self setAssociatedNSNumber:@(number) withIdentifier:identifier];
}

-(void)setFloat:(CGFloat)number withIdentifier:(char *)identifier
{
    [self setAssociatedNSNumber:@(number) withIdentifier:identifier];
}

#pragma makr - primitives getters

-(BOOL)booleanForIdentifier:(char *)identifier defoult:(BOOL)defoult
{
    NSNumber *number = [self associatedNsnuberFromIdentifier:identifier];
    return (number) ? [number boolValue] : defoult;
}

-(NSInteger)integerForIdentifier:(char *)identifier defoult:(NSInteger)defoult
{
    NSNumber *number = [self associatedNsnuberFromIdentifier:identifier];
    return (number) ? [number integerValue] : defoult;
}

-(CGFloat)floatForIdentifier:(char *)identifier defoult:(CGFloat)defoult
{
    NSNumber *number = [self associatedNsnuberFromIdentifier:identifier];
    return (number) ? [number floatValue] : defoult;
}

@end

@implementation UIView (StoryboardAnimations)

@dynamic doNotAnimate, translationDirection, degrees, scaleFactor, orderNumber, animateSubviews;

-(void)setDoNotAnimate:(BOOL)doNotAnimate
{
    [self setBoolean:doNotAnimate withIdentifier:&kAssociatedDoNotAnimate];
}

-(BOOL)doNotAnimate
{
    return [self booleanForIdentifier:&kAssociatedDoNotAnimate defoult:NO];
}

-(void)setTranslationDirection:(TranslationDirection)translationDirection
{
    [self setInteger:translationDirection withIdentifier:&kAssociatedTranslationDirection];
}

-(TranslationDirection)translationDirection
{
    return [self integerForIdentifier:&kAssociatedTranslationDirection defoult:5];
}

-(void)setDegrees:(CGFloat)degrees
{
    [self setFloat:degrees withIdentifier:&kAssociatedDegrees];
}

-(CGFloat)degrees
{
    return [self floatForIdentifier:&kAssociatedDegrees defoult:0];
}

-(void)setScaleFactor:(CGFloat)scaleFactor
{
    [self setFloat:scaleFactor withIdentifier:&kAssociatedScaleFactor];
}

-(CGFloat)scaleFactor
{
    return [self floatForIdentifier:&kAssociatedScaleFactor defoult:1];
}

-(void)setOrderNumber:(NSInteger)orderNumber
{
    [self setInteger:orderNumber withIdentifier:&kAssociatedOrderNumber];
}

-(NSInteger)orderNumber
{
    return [self integerForIdentifier:&kAssociatedOrderNumber defoult:0];
}

-(void)setAnimateSubviews:(BOOL)animateSubviews
{
    [self setBoolean:animateSubviews withIdentifier:&kAssociatedAnimateSubviews];
}

-(BOOL)animateSubviews
{
    return [self booleanForIdentifier:&kAssociatedAnimateSubviews defoult:NO];
}

@end
