//
//  SATypeHide.h
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/11/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAType.h"

@interface SATypeHide : NSObject <SAType>

@end
