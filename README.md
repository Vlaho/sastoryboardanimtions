# SAStoryboardAnimtions

[![CI Status](http://img.shields.io/travis/Vlaho/SAStoryboardAnimtions.svg?style=flat)](https://travis-ci.org/Vlaho/SAStoryboardAnimtions)
[![Version](https://img.shields.io/cocoapods/v/SAStoryboardAnimtions.svg?style=flat)](http://cocoapods.org/pods/SAStoryboardAnimtions)
[![License](https://img.shields.io/cocoapods/l/SAStoryboardAnimtions.svg?style=flat)](http://cocoapods.org/pods/SAStoryboardAnimtions)
[![Platform](https://img.shields.io/cocoapods/p/SAStoryboardAnimtions.svg?style=flat)](http://cocoapods.org/pods/SAStoryboardAnimtions)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SAStoryboardAnimtions is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SAStoryboardAnimtions'
```

```ruby
source 'https://bitbucket.org/infinum_hr/cocoapods.git'
```

To work with other pods also add:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
```

## Author

Vlaho, vlaho.poluta@infinum.hr

## License

SAStoryboardAnimtions is available under the MIT license. See the LICENSE file for more info.
