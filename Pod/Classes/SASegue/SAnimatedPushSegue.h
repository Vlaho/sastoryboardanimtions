//
//  SAPushSegue.h
//  StoryboardAnimtions
//
//  Created by Vlaho Poluta on 8/11/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SAnimatedPushSegue : UIStoryboardSegue

@end
